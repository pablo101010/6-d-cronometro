
// Cronómetro

// Realizar una web con un cronómetro, que tenga las opciones de iniciar, reset (volver el cronómetro a 0) y pausar.
// obtenerMinutos();

var milisegundosPc;
var band = 0;
var refrescoPantalla;
var contador = 0;
var miliSeg= 1;
var contSeg = 0;
var contMin = 0;
var bandera = 0;


milisegundosPc = document.getElementById("milisegundos");
segundosPc = document.getElementById("segundos");
minutosPc = document.getElementById("minutos");

milisegundosPc.innerText = "00";
segundosPc.innerText = "00";
minutosPc.innerText = "00";

function iniciando() {
    refrescoPantalla = window.setInterval(escribir, 100);

}


function escribir() {

    //Milisegundos
    contador = contador + 1;
    if (contador < 10) {
        miliSeg = ("0" + contador);
        bandera=0;
    } else {
        contador = 0;
        miliSeg = ("00");
        bandera=1;
    }
    milisegundosPc.innerText = miliSeg;


    //Segundos
    if (miliSeg == 0) {
        contSeg = contSeg + 1;
        if (contSeg < 60) {
            if (contSeg < 10) {
                segundosPc.innerText = ("0" + contSeg);
            } else {
                segundosPc.innerText = contSeg;
                //bandera=1;
            }
        } else {
            contSeg = 0;
            segundosPc.innerText = "00";
        }
    }

    //Minutos
    if (bandera == 1) {
        if (contSeg == 0) {
            contMin = contMin + 1;
            if (contMin < 60) {
                if (contMin < 10) {
                    minutosPc.innerText = ("0" + contMin);
                } else {
                    minutosPc.innerText = contMin;
                }
            } else {
                contMin = 0;
                minutosPc.innerText = "00";
            }
        }
    }
}

function parando() {
    clearInterval(refrescoPantalla);
}

function reiniciando() {
    milisegundosPc.innerText = "00";
    segundosPc.innerText = "00";
    minutosPc.innerText = "00";

    contador=0;
    contSeg=0;
    contMin=0;
}